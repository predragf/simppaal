package org.fmaes.simppaal.simulinktotimedautomata.platformextender;

import org.fmaes.simppaal.simulinktotimedautomata.core.types.SimulinkBlockWrapper;

public interface BlockRoutineGeneratorInterface {

  public String generateBlockRoutine(SimulinkBlockWrapper blockForParsing);

  public String generateSignalDeclaration(SimulinkBlockWrapper blockForParsing);

  public String generateInitRoutine(SimulinkBlockWrapper blockForParsing);

  public String generateDeclaration(SimulinkBlockWrapper blockForParsing);

  public String generateDafnyVerificationRoutine(SimulinkBlockWrapper blockForParsing);
}

