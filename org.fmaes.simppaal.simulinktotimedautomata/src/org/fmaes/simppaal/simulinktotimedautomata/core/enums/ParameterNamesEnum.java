/**
 * 
 */
package org.fmaes.simppaal.simulinktotimedautomata.core.enums;

/**
 * @author Predrag Filipovikj (predrag.filipovikj@mdh.se)
 *
 */
public enum ParameterNamesEnum {
  DISK_LOCATION {
    @Override
    public String toString() {
      return "disklocation";
    }
  },

}
